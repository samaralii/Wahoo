package com.shaheersolution.wahoo

import com.shaheersolution.wahoo.di.modules.RxSchedulers
import io.reactivex.schedulers.Schedulers

class RxTestSchedulersImpl : RxSchedulers {
    override fun io() = Schedulers.trampoline()
    override fun computation() = Schedulers.trampoline()
    override fun ui() = Schedulers.trampoline()
}