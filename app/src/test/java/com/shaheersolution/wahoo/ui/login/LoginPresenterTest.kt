package com.shaheersolution.wahoo.ui.login

import com.shaheersolution.wahoo.RxTestSchedulersImpl
import com.shaheersolution.wahoo.data.models.User
import com.shaheersolution.wahoo.data.models.UserLogin
import com.shaheersolution.wahoo.data.source.TaskRepository
import io.reactivex.Single
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.junit.MockitoJUnit


class LoginPresenterTest {

    @Rule
    @JvmField
    public val mockitoRule = MockitoJUnit.rule()

    @Mock
    lateinit var view: LoginView

    @Mock
    lateinit var repository: TaskRepository

    lateinit var presenter: LoginPresenter
    lateinit var schedulers: RxTestSchedulersImpl

    val LOGIN_SUCCESS = "success"
    val LOGIN_FAILED = "failed"


    @Before
    fun setUp() {
        schedulers = RxTestSchedulersImpl()
        presenter = LoginPresenter(repository, schedulers)
        presenter.attachView(view)
    }


    @Test
    fun shouldSuccessfullyLoggedIn() {
        val user = Mockito.mock(User::class.java)
        val userLogin = UserLogin(LOGIN_SUCCESS, user)

        Mockito.`when`(repository.login(Mockito.anyString(), Mockito.anyString())).thenReturn(Single.just(userLogin))

        presenter.login(Mockito.anyString(), Mockito.anyString())

        Mockito.verify(view).onSuccessLogin(userLogin.user)
    }

    @Test
    fun shouldFailLogIn() {
        val user = Mockito.mock(User::class.java)
        val userLogin = UserLogin(LOGIN_FAILED, user)

        Mockito.`when`(repository.login(Mockito.anyString(), Mockito.anyString())).thenReturn(Single.just(userLogin))

        presenter.login(Mockito.anyString(), Mockito.anyString())

        Mockito.verify(view).onErrorLogin()

    }


}