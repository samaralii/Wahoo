package com.shaheersolution.wahoo

import android.app.Application
import com.shaheersolution.wahoo.di.AppComponent
import com.shaheersolution.wahoo.di.DaggerAppComponent
import com.shaheersolution.wahoo.di.modules.*
import com.shaheersolution.wahoo.util.AppConsts

class App : Application() {

    lateinit var component: AppComponent
        private set


    override fun onCreate() {
        super.onCreate()
        initDagger()
    }

    private fun initDagger() {
        component = DaggerAppComponent.builder().apply {
            netModule(NetModule(AppConsts.BASE_URL))
            appModule(AppModule(this@App))
            schedulersModule(SchedulersModule())
            repositoryModule(RepositoryModule())
            sharedPreferenceModule(SharedPreferenceModule())
        }.build()

        component.Inject(this)
    }
}