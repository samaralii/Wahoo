package com.shaheersolution.wahoo.data.models

import android.os.Parcel
import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Invoice(val id: String = "", val invoiceNumber: String = "", val receiverName: String = "",
              val contactNo: String = "", val airwaybill_id: String? = null, val sub_company_id: String? = null,
              val barcode: String? = null, val receiverAddress: String? = null, val country: String? = null,
              val item: String? = null, val city: String? = null, val price: Double? = null, val date: String? = null,
              val status: Int? = null, val created: String? = null, val modified: String? = null,
              val pickupDateTime: String? = null, val diliveryCharges: Double? = null, val note: String? = null,
              val senderName: String? = null, val senderContactNo: String? = null) : Parcelable




