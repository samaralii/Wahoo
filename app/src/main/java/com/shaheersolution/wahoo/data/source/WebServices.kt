package com.shaheersolution.wahoo.data.source

import com.shaheersolution.wahoo.data.models.*
import io.reactivex.Observable
import io.reactivex.Single
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Response
import retrofit2.http.*

interface WebServices {

    @FormUrlEncoded
    @POST("users/applogin")
    fun login(@Field("email") email: String, @Field("password") password: String): Single<UserLogin>

    @FormUrlEncoded
    @POST("users/applogin")
    fun newlogin(@Field("email") email: String, @Field("password") password: String): Observable<UserLogin>

    @FormUrlEncoded
    @POST("invoices/getinvoice")
    fun getInvoice(@Field("barcode") barcode: String): Single<Response<GetInvoice>>

    @FormUrlEncoded
    @POST("invoices/saveinvoices")
    fun confirmPickups(@Field("InvoicesIds") json: String): Single<Response<ApiResponse>>

    @FormUrlEncoded
    @POST("invoices/deliveries")
    fun getDeliveriesList(@Field("user_id") id: String): Single<Response<GetDeliveries>>

    @Multipart
    @POST("invoices/updatestatus")
    fun approveInvoice(@Part("invoiceId") invoiceId: RequestBody, @Part("sub_company_id") sub_company_id: RequestBody,
                       @Part("status") status: RequestBody, @Part("invoiceNumber") invoiceNumber: RequestBody, @Part signature: MultipartBody.Part):
            Single<Response<UpdateInvoice>>

    @FormUrlEncoded
    @POST("invoices/updatestatus")
    fun cancelInvoice(@Field("invoiceId") invoiceId: String, @Field("sub_company_id") sub_company_id: String,
                      @Field("status") status: String, @Field("note") note: String, @Field("invoiceNumber") invoiceNumber: String):
            Single<Response<UpdateInvoice>>

    @FormUrlEncoded
    @POST("invoices/onhold")
    fun getHoldList(@Field("user_id") id: String): Single<Response<GetDeliveries>>

}