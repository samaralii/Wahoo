package com.shaheersolution.wahoo.data.models

class GetDeliveries(val response: String, val invoices: List<Invoice>? = null)