package com.shaheersolution.wahoo.data.source

import android.annotation.SuppressLint
import android.content.SharedPreferences
import com.google.gson.Gson
import com.shaheersolution.wahoo.data.models.*
import com.shaheersolution.wahoo.util.AppConsts
import io.reactivex.Observable
import io.reactivex.Single
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Retrofit
import javax.inject.Singleton

@Singleton
@SuppressLint("ApplySharedPref")
class TaskRepository(private val retrofit: Retrofit,
                     private val sharedPreferences: SharedPreferences,
                     private val gson: Gson) : TaskDataSource {


    private val api: WebServices = retrofit.create(WebServices::class.java)

    @SuppressLint("CommitPrefEdits")
    override fun saveUserData(user: User) {
        val json = gson.toJson(user)
        sharedPreferences.edit().apply {
            putString(AppConsts.USER_DATA, json)
            apply()
        }
    }

    override fun getUserData(): User? {
        val json = getDataByKey(AppConsts.USER_DATA)
        return if (json != null) {
            gson.fromJson(json, User::class.java)
        } else null

    }

    override fun getDataByKey(key: String): String? = sharedPreferences.getString(key, null)

    @SuppressLint("CommitPrefEdits")
    override fun clearCache() {
        sharedPreferences.edit().apply {
            clear()
            apply()
        }
    }

    override fun login(email: String, password: String): Single<UserLogin> {
        return api.login(email, password)
    }

    override fun newlogin(email: String, password: String): Observable<User> {
        return api.newlogin(email, password).map {
            it.user
        }
    }

    override fun getInvoice(barcode: String): Single<GetInvoice> {
        return api.getInvoice(barcode).map { it.body() }
    }

    override fun confirmPickups(json: String): Single<ApiResponse> {
        return api.confirmPickups(json).map { it.body() }
    }

    override fun getDeliveriesList(id: String): Single<GetDeliveries> =
            api.getDeliveriesList(id).map { it.body() }

    override fun getHoldList(id: String): Single<GetDeliveries> =
            api.getHoldList(id).map { it.body() }

    override fun approveInvoice(invoiceId: RequestBody, sub_company_id: RequestBody,
                                status: RequestBody, invoiceNumber: RequestBody, signature: MultipartBody.Part): Single<UpdateInvoice> {
        return api.approveInvoice(invoiceId, sub_company_id, status, invoiceNumber, signature).map { it.body() }
    }

    override fun cancelInvoice(invoiceId: String, sub_company_id: String,
                               status: String, note: String, invoiceNumber: String): Single<UpdateInvoice> {
        return api.cancelInvoice(invoiceId, sub_company_id, status, note, invoiceId).map { it.body() }
    }

}