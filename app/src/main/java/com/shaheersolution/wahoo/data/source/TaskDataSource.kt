package com.shaheersolution.wahoo.data.source

import com.shaheersolution.wahoo.data.models.*
import io.reactivex.Observable
import io.reactivex.Single
import okhttp3.MultipartBody
import okhttp3.RequestBody

interface TaskDataSource {

    fun getUserData(): User?
    fun saveUserData(user: User)
    fun getDataByKey(key: String): String?
    fun clearCache()

    fun login(email: String, password: String): Single<UserLogin>
    fun newlogin(email: String, password: String): Observable<User>

    fun getInvoice(barcode: String): Single<GetInvoice>

    fun confirmPickups(json: String): Single<ApiResponse>

    fun getDeliveriesList(id: String): Single<GetDeliveries>
    fun getHoldList(id: String): Single<GetDeliveries>

    fun approveInvoice(invoiceId: RequestBody, sub_company_id: RequestBody,
                       status: RequestBody, invoiceNumber: RequestBody, signature: MultipartBody.Part): Single<UpdateInvoice>

    fun cancelInvoice(invoiceId: String, sub_company_id: String,
                      status: String, note: String, invoiceNumber: String): Single<UpdateInvoice>

}