package com.shaheersolution.wahoo.data.models

class User(val id: String, val company_id: String, val sub_company_id: String,
           val first_name: String, val last_name: String, val username: String,
           val email: String, val role: String, val phone: String, val city: String,
           val state: String, val zip: String, val country: String)
