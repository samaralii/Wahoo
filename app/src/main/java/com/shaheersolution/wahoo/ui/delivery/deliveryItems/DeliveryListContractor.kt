package com.shaheersolution.wahoo.ui.delivery.deliveryItems

import android.app.Activity
import android.content.Intent
import com.hannesdorfmann.mosby3.mvp.MvpBasePresenter
import com.hannesdorfmann.mosby3.mvp.MvpView
import com.shaheersolution.wahoo.data.models.GetDeliveries
import com.shaheersolution.wahoo.data.models.Invoice
import com.shaheersolution.wahoo.data.source.TaskDataSource
import com.shaheersolution.wahoo.di.modules.RxSchedulers
import com.shaheersolution.wahoo.ui.detail.DetailActivity
import com.shaheersolution.wahoo.util.AppConsts
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.observers.DisposableSingleObserver


class DeliveryListPresenter(private val repo: TaskDataSource, private val schedulers: RxSchedulers) : MvpBasePresenter<DeliveryListView>() {

    private val disposable = CompositeDisposable()

    fun getItemsList() {

        val id = repo.getUserData()?.id

        if (id != null)
            disposable.add(repo.getDeliveriesList(id)
                    .subscribeOn(schedulers.io())
                    .observeOn(schedulers.ui())
                    .doFinally { if (isViewAttached) view?.dismissProgress() }
                    .subscribeWith(object : DisposableSingleObserver<GetDeliveries>() {
                        override fun onSuccess(t: GetDeliveries) {

                            try {

                                if (t.response == "success") {

                                    t.invoices?.let {
                                        if (isViewAttached)
                                            sortList(it)
                                    } ?: if (isViewAttached) view?.onDeliveriesNotFound()


                                } else {
                                    if (isViewAttached) view?.onDeliveriesNotFound()
                                }

                            } catch (e: Exception) {
                                e.printStackTrace()
                                if (isViewAttached) view?.onErrorGettingDeliveryList()
                            }


                        }

                        override fun onError(e: Throwable) {
                            e.printStackTrace()
                            if (isViewAttached) view?.onErrorGettingDeliveryList()
                        }

                    }))

    }

    private fun sortList(it: List<Invoice>) {

        val sortedList = it.sortedWith(Comparator { o1, o2 ->
            o1.city?.compareTo(o2.city.toString(), true) ?: 0
        })

       view?.onSuccessDeliveryList(sortedList.toMutableList())
    }

    fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (resultCode == Activity.RESULT_OK) {
            when (requestCode) {
                DetailActivity.DETAIL_ACTIVITY_REQUEST_CODE -> {
                    if (isViewAttached) view?.showProgress()
                    getItemsList()
                }

                AppConsts.BARCODE_ACTIVITY_REQUEST_CODE -> {
                    val code = data?.getStringExtra(AppConsts.BARCODE_VALUE)
                    if (code != null)
                        if (isViewAttached) view?.searchByCode(code)
                }
            }
        }
    }

    override fun detachView(retainInstance: Boolean) {
        super.detachView(retainInstance)
        if (!retainInstance) disposable.clear()
    }

}

interface DeliveryListView : MvpView {
    fun dismissProgress()
    fun onSuccessDeliveryList(list: MutableList<Invoice>)
    fun onErrorGettingDeliveryList()
    fun onDeliveriesNotFound()
    fun showProgress()
    fun searchByCode(code: String)
}