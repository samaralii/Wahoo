package com.shaheersolution.wahoo.ui.pickups

import android.app.Activity
import android.content.Intent
import com.hannesdorfmann.mosby3.mvp.MvpBasePresenter
import com.hannesdorfmann.mosby3.mvp.MvpView
import com.shaheersolution.wahoo.data.models.ApiResponse
import com.shaheersolution.wahoo.data.models.GetInvoice
import com.shaheersolution.wahoo.data.models.Invoice
import com.shaheersolution.wahoo.data.source.TaskDataSource
import com.shaheersolution.wahoo.di.modules.RxSchedulers
import com.shaheersolution.wahoo.util.AppConsts
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.observers.DisposableSingleObserver


class PickupsPresenter(private val repository: TaskDataSource, private val schedulers: RxSchedulers) : MvpBasePresenter<PickupsView>() {

    private val disposable = CompositeDisposable()

    override fun detachView(retainInstance: Boolean) {
        super.detachView(retainInstance)
        if (!retainInstance)
            disposable.clear()
    }

    fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (resultCode == Activity.RESULT_OK)
            if (requestCode == AppConsts.BARCODE_ACTIVITY_REQUEST_CODE) {
                val code = data?.getStringExtra(AppConsts.BARCODE_VALUE)
                if (code != null)
                    getInvoice(code)
                else
                    if (isViewAttached) view?.onCodeNull()
            }
    }

    private fun getInvoice(code: String) {

        if (isViewAttached) view?.showProgress()
        disposable.add(repository.getInvoice(code)
                .subscribeOn(schedulers.io())
                .observeOn(schedulers.ui())
                .doFinally { if (isViewAttached) view?.dismissProgress() }
                .subscribeWith(object : DisposableSingleObserver<GetInvoice>() {
                    override fun onError(e: Throwable) {
                        e.printStackTrace()
                        if (isViewAttached) view?.onErrorGettingInvoice()
                    }

                    override fun onSuccess(t: GetInvoice) {
                        try {

                            if (t.response == "success") {
                                if (isViewAttached) view?.viewList()
                                if (isViewAttached) view?.onSuccessGetInvoice(t.invoice!!)
                            } else {
                                if (isViewAttached) view?.onNoInvoiceFound()
                            }
                        } catch (e: Exception) {
                            e.printStackTrace()
                            if (isViewAttached) view?.onErrorGettingInvoice()
                        }

                    }

                }))
    }

    fun confirmPickups(json: String) {
        disposable.add(repository.confirmPickups(json)
                .subscribeOn(schedulers.io())
                .observeOn(schedulers.ui())
                .doFinally { if (isViewAttached) view?.dismissProgress() }
                .subscribeWith(object : DisposableSingleObserver<ApiResponse>() {
                    override fun onError(e: Throwable) {
                        e.printStackTrace()
                        if (isViewAttached) view?.onErrorConfirmPickups()
                    }

                    override fun onSuccess(t: ApiResponse) {
                        try {

                            if (t.response == "success") {
                                if (isViewAttached) view?.onSuccessConfirmPickups()
                            } else
                                if (isViewAttached) view?.onErrorConfirmPickups()


                        } catch (e: Exception) {
                            e.printStackTrace()
                            if (isViewAttached) view?.onErrorConfirmPickups()
                        }
                    }

                }))
    }
}


interface PickupsView : MvpView {
    fun onCodeNull()
    fun viewList()
    fun onSuccessGetInvoice(invoice: Invoice)
    fun onErrorGettingInvoice()
    fun dismissProgress()
    fun onNoInvoiceFound()
    fun showProgress()
    fun onErrorConfirmPickups()
    fun onSuccessConfirmPickups()
}