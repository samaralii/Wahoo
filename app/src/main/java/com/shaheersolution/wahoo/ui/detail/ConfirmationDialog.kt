package com.shaheersolution.wahoo.ui.detail

import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.shaheersolution.wahoo.R
import com.shaheersolution.wahoo.util.AppConsts
import com.shaheersolution.wahoo.util.goneView
import com.shaheersolution.wahoo.util.showView
import kotlinx.android.synthetic.main.dialog_confirmation.*

class ConfirmationDialog : DialogFragment() {

    private var listener: DetailDialogListener? = null
    private var status = 0

    companion object {
        private val STATUS = "status"
        fun newInstance(status: Int) = ConfirmationDialog().apply {
            arguments = Bundle().apply {
                putInt(STATUS, status)
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        try {
            listener = parentFragment as DetailFragment
        } catch (e: Exception) {
            throw Exception("Implement Listener")
        }

        status = arguments?.getInt(STATUS, 0)!!

    }

    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?): View? =
            inflater.inflate(R.layout.dialog_confirmation, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        init()
    }

    private fun init() {
        dialog.setTitle("Confirmation")

        when (status) {
            AppConsts.Status.ID_CANCELED -> {
                dialog_confirmation_btnCancel.showView()
                dialog_confirmation_btnHold.goneView()
            }
            AppConsts.Status.ID_ON_HOLD -> {
                dialog_confirmation_btnCancel.goneView()
                dialog_confirmation_btnHold.showView()
            }

            else -> dismiss()
        }

        dialog_confirmation_btnCancel.setOnClickListener { changeStatus() }
        dialog_confirmation_btnHold.setOnClickListener { changeStatus() }
    }

    private fun changeStatus() {
        if (dialog_confirmation_etReason.text.toString().isEmpty()) {
            dialog_confirmation_tilReason.error = "Required Field"
            return
        } else
            dialog_confirmation_tilReason.error = null


        listener?.onClick(dialog_confirmation_etReason.text.toString(), status)
        dialog.dismiss()
    }

}