package com.shaheersolution.wahoo.ui.detail

import com.shaheersolution.wahoo.util.AppConsts

interface DetailDialogListener {
    fun onClick(note: String, status: Int)
}