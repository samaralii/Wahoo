package com.shaheersolution.wahoo.ui.delivery.holdItems

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.shaheersolution.wahoo.R
import com.shaheersolution.wahoo.data.models.Invoice


class HoldListAdapter(private var data: MutableList<Invoice> = arrayListOf(),
                      val onItemClick: (invoice: Invoice) -> Unit,
                      val onItemNotFound: () -> Unit) : RecyclerView.Adapter<HoldListVH>() {

    override fun getItemCount() = data.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HoldListVH {
        return HoldListVH(LayoutInflater.from(parent.context).inflate(R.layout.list_delivery_new, parent, false))
    }

    override fun onBindViewHolder(holder: HoldListVH, position: Int) {

        holder.tvCity?.text = data[position].city
        holder.tvInvoiceNumber?.text = data[position].barcode
        holder.tvContactNumber?.text = data[position].contactNo
        holder.tvAddress?.text = data[position].receiverAddress

        holder.itemView?.setOnClickListener {
            onItemClick.invoke(data[holder.layoutPosition])
        }
    }

    fun addItems(newData: MutableList<Invoice> = arrayListOf()) {
        data.clear()
        data = newData
        notifyDataSetChanged()
    }

    fun searchItem(code: String) {
        (0 until data.size).firstOrNull { code == data[it].barcode }?.let {
            onItemClick.invoke(data[it])
        } ?: onItemNotFound.invoke()
    }
}


class HoldListVH(v: View) : RecyclerView.ViewHolder(v) {
    val tvInvoiceNumber by lazy { v.findViewById<TextView>(R.id.list_delivery_tvInvoiceNumber) }
    val tvCity by lazy { v.findViewById<TextView>(R.id.list_delivery_tvCity) }
    val tvContactNumber by lazy { v.findViewById<TextView>(R.id.list_delivery_tvContactNumber) }
    val tvAddress by lazy { v.findViewById<TextView>(R.id.list_delivery_tvAddress) }

}