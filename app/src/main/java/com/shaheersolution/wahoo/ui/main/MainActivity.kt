package com.shaheersolution.wahoo.ui.main

import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.view.Menu
import android.view.MenuItem
import com.shaheersolution.wahoo.BaseActivity
import com.shaheersolution.wahoo.R
import com.shaheersolution.wahoo.data.models.User
import com.shaheersolution.wahoo.data.source.TaskRepository
import com.shaheersolution.wahoo.ui.delivery.DeliveryFragment
import com.shaheersolution.wahoo.ui.delivery.deliveryItems.DeliveryListFragment
import com.shaheersolution.wahoo.ui.login.LoginActivity
import com.shaheersolution.wahoo.ui.pickups.PickupsFragment
import com.shaheersolution.wahoo.util.addFragmentToActivity
import com.shaheersolution.wahoo.util.replaceFragmentToActivity
import com.shaheersolution.wahoo.util.toastInfo
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.include_toolbar.*
import javax.inject.Inject

class MainActivity : BaseActivity() {

    @Inject
    lateinit var repository: TaskRepository

    private var user: User? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        gifApplication().component.Inject(this)
        init()
    }


    private fun init() {

        setSupportActionBar(app_toolbar)

        activity_main_btmNavView.setOnNavigationItemSelectedListener { item ->
            navOnClick(item.itemId)
            true
        }

        PickupsFragment.newInstance().addFragmentToActivity(supportFragmentManager, R.id.activity_main_container)
    }

    private fun navOnClick(id: Int) {

        val currentFragment = supportFragmentManager.findFragmentById(R.id.activity_main_container)

        when (id) {
            R.id.action_1 -> if (currentFragment !is PickupsFragment) replaceFragment(PickupsFragment.newInstance())
            R.id.action_2 -> if (currentFragment !is DeliveryListFragment) replaceFragment(DeliveryFragment.newInstance())
        }
    }

    private fun replaceFragment(fragment: Fragment) {
        supportFragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE)
        fragment.replaceFragmentToActivity(supportFragmentManager, R.id.activity_main_container)
    }


    fun getUser(): User? {
        if (user == null)
            user = repository.getUserData()

        return user
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_detail, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {

        item?.itemId?.let {

            when(it) {
                R.id.action_logout -> {
                    repository.clearCache()
                    startActivity(Intent(this, LoginActivity::class.java))
                    finish()
                }
            }

        }

        return super.onOptionsItemSelected(item)
    }


    private var isBackPress = true
    override fun onBackPressed() {
        val c = supportFragmentManager.backStackEntryCount

        if (c > 0) {
            supportFragmentManager.popBackStack()
        } else {

            if (isBackPress) {
                toastInfo("Press Back Again To Exit")
                isBackPress = false
            } else {
                super.onBackPressed()
            }
        }
    }

}
