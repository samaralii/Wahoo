package com.shaheersolution.wahoo.ui.pickups

import android.app.ProgressDialog
import android.content.Intent
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.hannesdorfmann.mosby3.mvp.MvpFragment
import com.shaheersolution.wahoo.R
import com.shaheersolution.wahoo.data.models.Invoice
import com.shaheersolution.wahoo.data.source.TaskRepository
import com.shaheersolution.wahoo.di.modules.RxSchedulersImpl
import com.shaheersolution.wahoo.ui.barcodeScanner.BarcodeScannerActivity
import com.shaheersolution.wahoo.ui.main.MainActivity
import com.shaheersolution.wahoo.util.*
import kotlinx.android.synthetic.main.frag_pickups.*
import javax.inject.Inject

class PickupsFragment : MvpFragment<PickupsView, PickupsPresenter>(), PickupsView {

    @Inject
    lateinit var repository: TaskRepository

    @Inject
    lateinit var schedulers: RxSchedulersImpl

    var dialog: ProgressDialog? = null

    private var adapter: PickupsAdapter? = null

    override fun createPresenter(): PickupsPresenter {
        (activity as MainActivity).gifApplication().component.Inject(this)
        return PickupsPresenter(repository, schedulers)
    }

    companion object {
        fun newInstance() = PickupsFragment()
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?)
            = inflater.inflate(R.layout.frag_pickups, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        dialog = ProgressDialog(context)
        dialog?.setMessage("Loading...")

        (activity as MainActivity)._setTitle(R.string.title_pickups)

        frag_pickups_list.layoutManager = LinearLayoutManager(context)

        frag_pickups_btnScanner.setOnClickListener { openBarcodeActivity() }
        frag_pickups_btnSubmit.setOnClickListener { confirmPickups() }

    }

    private fun confirmPickups() {

        adapter?.getItems()?.let {
//            frag_pickups_progressBar.showView()
            dialog?.show()
            presenter.confirmPickups(it)
        } ?: toastError("Error")
    }

    private fun openBarcodeActivity() {
        val intent = Intent(context, BarcodeScannerActivity::class.java)
        startActivityForResult(intent, AppConsts.BARCODE_ACTIVITY_REQUEST_CODE)
    }

    override fun onCodeNull() {
        toastError("Error")
    }

    override fun viewList() {
        frag_pickups_list.showView()
        frag_pickups_NoItem.goneView()
    }

    override fun onSuccessGetInvoice(invoice: Invoice) {

        if (adapter == null) {

            adapter = PickupsAdapter(mutableListOf(invoice),
                    showEmptyView = {
                        frag_pickups_list.goneView()
                        frag_pickups_NoItem.showView()
                        frag_pickups_btnSubmit.isEnabled = false
                    },
                    itemAlreadyAdded = {
                        toastInfo("Item Already Added")
                    })

            frag_pickups_list.adapter = adapter
            frag_pickups_btnSubmit.isEnabled = true


        } else {
            adapter?.addItem(invoice)
            frag_pickups_btnSubmit.isEnabled = true
        }

    }

    override fun onErrorGettingInvoice() {
        toastError("Error")
    }

    override fun dismissProgress() {
//        frag_pickups_progressBar.hideView()
        dialog?.dismiss()
    }

    override fun showProgress() {
//        frag_pickups_progressBar.showView()
        dialog?.show()
    }

    override fun onNoInvoiceFound() {
        toastError("Invoice Not Found")
    }

    override fun onErrorConfirmPickups() {
        toastError("Error")
    }

    override fun onSuccessConfirmPickups() {
        adapter?.deleteAllItems()
        toastSuccess("Done")
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        presenter.onActivityResult(requestCode, resultCode, data)
    }
}