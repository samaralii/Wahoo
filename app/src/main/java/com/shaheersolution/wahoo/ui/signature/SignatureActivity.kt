package com.shaheersolution.wahoo.ui.signature

import android.app.Activity
import android.content.Intent
import android.graphics.Bitmap
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import com.github.gcacace.signaturepad.views.SignaturePad
import com.shaheersolution.wahoo.R
import com.shaheersolution.wahoo.util.AppConsts
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_signature.*
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*


class SignatureActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_signature)
        init()
    }

    private fun init() {
        activity_signature_btnClear.setOnClickListener { onClear() }
        activity_signature_btnOk.setOnClickListener { onOk() }
        signature_pad.setOnClickListener { signatureListener }
    }

    private fun onClear() {
        signature_pad.clear()
    }

    private fun onOk() {
        saveSignature()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { imageName ->

                    val i = Intent()
                    i.putExtra(AppConsts.FILENAME, imageName)
                    setResult(Activity.RESULT_OK, i)
                    finish()

                }
    }


    private fun saveSignature(): Single<String> {
        return Single.create {

            var out: FileOutputStream? = null

            try {

                val b = signature_pad.signatureBitmap

                val file = cacheDir
                val timeStamp = SimpleDateFormat("yyyyMMddHHmmss").format(Calendar.getInstance().time)
                val imageName = "signature-$timeStamp.png"
                Log.d("IMAGE_NAME ->>>", imageName)
                val signatureFile = File(file, imageName)

                out = FileOutputStream(signatureFile)
                b.compress(Bitmap.CompressFormat.PNG, 100, out)
                it.onSuccess(imageName)

            } catch (e: IOException) {
                it.onError(e)
            } finally {
                out?.close()
            }

        }
    }


    private val signatureListener = object : SignaturePad.OnSignedListener {
        override fun onStartSigning() {
        }

        override fun onClear() {
        }

        override fun onSigned() {
        }

    }

}