package com.shaheersolution.wahoo.ui.detail

import android.os.Bundle
import com.shaheersolution.wahoo.BaseActivity
import com.shaheersolution.wahoo.R
import com.shaheersolution.wahoo.util.addFragmentToActivity

class DetailActivity : BaseActivity() {

    companion object {
        val DETAIL_ACTIVITY_REQUEST_CODE = 1
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)
        DetailFragment.newInstance(intent.getParcelableExtra(DetailFragment.INVOICE),
                intent.getIntExtra(DetailFragment.STATUS, 0))
                .addFragmentToActivity(supportFragmentManager, R.id.activity_detail_container)
    }
}
