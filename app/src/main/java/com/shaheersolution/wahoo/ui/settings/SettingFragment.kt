package com.shaheersolution.wahoo.ui.settings

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.shaheersolution.wahoo.R
import com.shaheersolution.wahoo.ui.main.MainActivity

class SettingFragment: Fragment() {

    companion object {
        fun newInstance() = SettingFragment()
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
            inflater.inflate(R.layout.frag_setting, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        init()
    }

    private fun init() {
        (activity as MainActivity)._setTitle(R.string.title_setting)
    }


}