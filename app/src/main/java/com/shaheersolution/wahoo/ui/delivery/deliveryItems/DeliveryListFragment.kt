package com.shaheersolution.wahoo.ui.delivery.deliveryItems

import android.content.Intent
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.hannesdorfmann.mosby3.mvp.MvpFragment
import com.shaheersolution.wahoo.R
import com.shaheersolution.wahoo.data.models.Invoice
import com.shaheersolution.wahoo.data.source.TaskRepository
import com.shaheersolution.wahoo.di.modules.RxSchedulersImpl
import com.shaheersolution.wahoo.ui.barcodeScanner.BarcodeScannerActivity
import com.shaheersolution.wahoo.ui.detail.DetailActivity
import com.shaheersolution.wahoo.ui.detail.DetailFragment
import com.shaheersolution.wahoo.ui.main.MainActivity
import com.shaheersolution.wahoo.util.AppConsts
import com.shaheersolution.wahoo.util.goneView
import com.shaheersolution.wahoo.util.showView
import com.shaheersolution.wahoo.util.toastError
import kotlinx.android.synthetic.main.frag_delivery_list.*
import javax.inject.Inject

class DeliveryListFragment : MvpFragment<DeliveryListView, DeliveryListPresenter>(), DeliveryListView {

    @Inject
    lateinit var repository: TaskRepository

    @Inject
    lateinit var schedulers: RxSchedulersImpl

    var adapter: DeliveryListAdapter? = null

    override fun createPresenter(): DeliveryListPresenter {
        (activity as MainActivity).gifApplication().component.Inject(this)
        return DeliveryListPresenter(repository, schedulers)
    }

    companion object {
        fun newInstance() = DeliveryListFragment()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
            inflater.inflate(R.layout.frag_delivery_list, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        init()
    }

    private fun init() {
        frag_delivery_list_recyclerView.layoutManager = LinearLayoutManager(context)

        adapter = DeliveryListAdapter(
                onItemClick = {
                    startActivityForResult(Intent(context, DetailActivity::class.java).apply {
                        putExtra(DetailFragment.INVOICE, it)
                        putExtra(DetailFragment.STATUS, AppConsts.Status.ID_PROCESSING)
                    }, DetailActivity.DETAIL_ACTIVITY_REQUEST_CODE)
                },
                onItemNotFound = {
                    toastError("Item Not Found")
                })

        frag_delivery_list_recyclerView.adapter = adapter
        refreshList()
        frag_delivery_list_sr.setOnRefreshListener { refreshList() }
        frag_delivery_list_search.setOnClickListener { onSearch() }


    }

    private fun onSearch() {
        startActivityForResult(Intent(context, BarcodeScannerActivity::class.java),
                AppConsts.BARCODE_ACTIVITY_REQUEST_CODE)
    }

    private fun refreshList() {
        frag_delivery_list_sr.isRefreshing = true
        presenter.getItemsList()
    }


    override fun onSuccessDeliveryList(list: MutableList<Invoice>) {
        frag_delivery_list_NoItem.goneView()
        adapter?.addItems(list)
    }

    override fun dismissProgress() {
        frag_delivery_list_sr.isRefreshing = false
    }

    override fun onErrorGettingDeliveryList() {
        toastError("Error")
    }

    override fun onDeliveriesNotFound() {
        frag_delivery_list_NoItem.showView()
        adapter?.addItems()
    }

    override fun showProgress() {
        frag_delivery_list_sr.isRefreshing = true
    }

    override fun searchByCode(code: String) {
        adapter?.searchItem(code)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        presenter.onActivityResult(requestCode, resultCode, data)
    }


}