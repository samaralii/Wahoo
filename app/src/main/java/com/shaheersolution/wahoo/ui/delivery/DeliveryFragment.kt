package com.shaheersolution.wahoo.ui.delivery

import android.os.Bundle
import android.support.design.widget.TabLayout
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentStatePagerAdapter
import android.view.*
import com.shaheersolution.wahoo.R
import com.shaheersolution.wahoo.ui.delivery.deliveryItems.DeliveryListFragment
import com.shaheersolution.wahoo.ui.delivery.holdItems.HoldListFragment
import com.shaheersolution.wahoo.ui.main.MainActivity
import kotlinx.android.synthetic.main.frag_delivery.*

class DeliveryFragment : Fragment() {

    companion object {
        fun newInstance() = DeliveryFragment()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
            inflater.inflate(R.layout.frag_delivery, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
//        setHasOptionsMenu(true)
        frag_delivery_tabLayout.apply {
            addTab(frag_delivery_tabLayout.newTab().setText("Delivery Items"))
            addTab(frag_delivery_tabLayout.newTab().setText("Hold Items"))
            tabGravity = TabLayout.GRAVITY_FILL
        }

        frag_delivery_viewPager.adapter = FragmentAdapter(fragmentManager!!, frag_delivery_tabLayout.tabCount)
        frag_delivery_viewPager.addOnPageChangeListener(TabLayout.TabLayoutOnPageChangeListener(frag_delivery_tabLayout))


        frag_delivery_tabLayout.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabReselected(tab: TabLayout.Tab?) {
            }

            override fun onTabUnselected(tab: TabLayout.Tab?) {
            }

            override fun onTabSelected(tab: TabLayout.Tab?) {
                frag_delivery_viewPager.currentItem = tab?.position!!
            }
        })
    }


//    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
//        inflater?.inflate(R.menu.menu_detail, menu)
//        super.onCreateOptionsMenu(menu, inflater)
//    }

    class FragmentAdapter(fm: FragmentManager, private val count: Int) : FragmentStatePagerAdapter(fm) {
        override fun getItem(position: Int) = when (position) {
            0 -> DeliveryListFragment.newInstance()
            1 -> HoldListFragment.newInstance()
            else -> null
        }
        override fun getCount() = count
    }


}