package com.shaheersolution.wahoo.ui.login

import com.hannesdorfmann.mosby3.mvp.MvpBasePresenter
import com.hannesdorfmann.mosby3.mvp.MvpView
import com.shaheersolution.wahoo.data.models.User
import com.shaheersolution.wahoo.data.models.UserLogin
import com.shaheersolution.wahoo.data.source.TaskDataSource
import com.shaheersolution.wahoo.di.modules.RxSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.observers.DisposableSingleObserver


class LoginPresenter(private val repository: TaskDataSource, private val scheduler: RxSchedulers) : MvpBasePresenter<LoginView>() {

    private val disposable by lazy { CompositeDisposable() }

    fun onCreate() {
        if (repository.getUserData() != null){
            if (isViewAttached) view.openMainActivity()
        }
    }

    fun login(email: String, password: String) {

        disposable.add(
                repository.login(email, password)
                        .subscribeOn(scheduler.io())
                        .observeOn(scheduler.ui())
                        .doFinally { if (isViewAttached) view?.dismissProgress() }
                        .subscribeWith(object : DisposableSingleObserver<UserLogin>() {
                            override fun onError(e: Throwable) {
                                e.printStackTrace()
                                if (isViewAttached) view?.onErrorLogin()
                            }

                            override fun onSuccess(t: UserLogin) {
                                try {
                                    if (t.response == "success") {
                                        repository.saveUserData(t.user)
                                        if (isViewAttached) view?.onSuccessLogin(t.user)
                                    } else {
                                        if (isViewAttached) view?.onErrorLogin()
                                    }
                                } catch (e: Exception) {
                                    e.printStackTrace()
                                    if (isViewAttached) view?.onErrorLogin()
                                }
                            }
                        }))
    }


    override fun detachView(retainInstance: Boolean) {
        super.detachView(retainInstance)
        if (!retainInstance) disposable.clear()
    }


}


interface LoginView : MvpView {
    fun dismissProgress()
    fun onErrorLogin()
    fun onSuccessLogin(user: User)
    fun openMainActivity()
}