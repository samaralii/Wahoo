package com.shaheersolution.wahoo.ui.detail

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.hannesdorfmann.mosby3.mvp.MvpFragment
import com.shaheersolution.wahoo.BaseActivity
import com.shaheersolution.wahoo.R
import com.shaheersolution.wahoo.data.models.Invoice
import com.shaheersolution.wahoo.data.source.TaskRepository
import com.shaheersolution.wahoo.di.modules.RxSchedulersImpl
import com.shaheersolution.wahoo.ui.signature.SignatureActivity
import com.shaheersolution.wahoo.util.*
import kotlinx.android.synthetic.main.frag_detail_new.*
import javax.inject.Inject

class DetailFragment : MvpFragment<DetailView, DetailPresenter>(), DetailView, DetailDialogListener {

    @Inject
    lateinit var repository: TaskRepository
    @Inject
    lateinit var schedulers: RxSchedulersImpl

    override fun createPresenter(): DetailPresenter {
        (activity as BaseActivity).gifApplication().component.Inject(this)
        return DetailPresenter(repository, schedulers)
    }

    var invoice: Invoice? = null
    var status = 0

    companion object {
        val INVOICE = "invoice"
        val STATUS = "status"
        fun newInstance(invoice: Invoice, status: Int) = DetailFragment().apply {
            arguments = Bundle().apply {
                putParcelable(INVOICE, invoice)
                putInt(STATUS, status)
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        invoice = arguments?.getParcelable(INVOICE)
        status = arguments?.getInt(STATUS, 0)!!
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
            inflater.inflate(R.layout.frag_detail_new, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        init()
    }

    private fun init() {

        if (status == AppConsts.Status.ID_ON_HOLD) {
            frag_detail_btnHold.goneView()
        }

        presenter.setInvoice(invoice, context!!.cacheDir)

        (activity as BaseActivity)._setTitle(title = invoice?.receiverName, subTitle = invoice?.city)

        frag_detail_tvPhoneNumber.text = invoice?.contactNo
        frag_detail_tvInvoiceNumber.text = invoice?.barcode
        frag_detail_tvAddress.text = invoice?.receiverAddress
        frag_detail_tvNote.text = invoice?.note
        frag_detail_tvSubCompanyName.text = invoice?.senderName
        frag_detail_tvItems.text = invoice?.item

        val price = invoice?.price?.let { it } ?: 0.0
        val charges = invoice?.diliveryCharges?.let { it } ?: 0.0

        frag_detail_tvPrice.text = "${price.plus(charges)}"

        frag_detail_btnApprove.setOnClickListener { onApprove() }
        frag_detail_btnCancel.setOnClickListener { onChangeStatus(AppConsts.Status.ID_CANCELED) }
        frag_detail_btnHold.setOnClickListener { onChangeStatus(AppConsts.Status.ID_ON_HOLD) }
        frag_detail_btnCall.setOnClickListener { onCall(invoice?.contactNo) }
        frag_detail_tvSubCompanyName.setOnClickListener { onCall(invoice?.senderContactNo) }

    }

    private fun onCall(number: String?) {
        if (number == null) return
        val intent = Intent(Intent.ACTION_DIAL)
        intent.data = Uri.parse("tel:$number")
        startActivity(intent)
    }

    private fun onChangeStatus(status: Int) {
        val dialog = ConfirmationDialog.newInstance(status)
        dialog.show(childFragmentManager, "confirmation_dialog")
    }

    override fun onClick(note: String, status: Int) {
        frag_detail_progressBar.showView()
        presenter.updateInvoiceStatus(note, status)
    }

    private fun onApprove() {
        startActivityForResult(Intent(context, SignatureActivity::class.java), AppConsts.SIGNATURE_ACTIVITY_REQUEST_CODE)
    }

    override fun showProgress() {
        frag_detail_progressBar.showView()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        presenter.onActivityResult(requestCode, resultCode, data)
    }

    override fun noInvoiceDataFound() {
    }

    override fun dismissProgress() {
        frag_detail_progressBar.goneView()
    }

    override fun onErrorApprovingInvoice() {
        toastError("Error")
    }

    override fun onSuccessApprovingInvoice() {
        toastSuccess("Done")
        finishActivity()
    }

    override fun onErrorStatusChanged() {
        toastError("Error")
    }

    override fun onSuccessStatusChanged() {
        toastSuccess("Done")
        finishActivity()
    }

    private fun finishActivity() {
        activity?.setResult(Activity.RESULT_OK)
        activity?.finish()
    }

}