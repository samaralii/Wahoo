package com.shaheersolution.wahoo.ui.login

import android.content.Intent
import android.os.Bundle
import com.daimajia.androidanimations.library.Techniques
import com.daimajia.androidanimations.library.YoYo
import com.hannesdorfmann.mosby3.mvp.MvpActivity
import com.shaheersolution.wahoo.App
import com.shaheersolution.wahoo.R
import com.shaheersolution.wahoo.data.models.User
import com.shaheersolution.wahoo.data.source.TaskRepository
import com.shaheersolution.wahoo.di.modules.RxSchedulersImpl
import com.shaheersolution.wahoo.ui.main.MainActivity
import com.shaheersolution.wahoo.util.*
import io.reactivex.Completable
import io.reactivex.android.schedulers.AndroidSchedulers
import kotlinx.android.synthetic.main.activity_login.*
import java.util.concurrent.TimeUnit
import javax.inject.Inject


class LoginActivity : MvpActivity<LoginView, LoginPresenter>(), LoginView {


    @Inject
    lateinit var repository: TaskRepository

    @Inject
    lateinit var schedulers: RxSchedulersImpl

    override fun createPresenter(): LoginPresenter {
        (application as App).component.Inject(this)
        return LoginPresenter(repository, schedulers)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        presenter.onCreate()
        Completable.timer(1, TimeUnit.SECONDS,
                AndroidSchedulers.mainThread())
                .subscribe {
                    startAnimation()
                }
        init()
    }

    override fun openMainActivity() {
        finish()
        startActivity(Intent(this, MainActivity::class.java))
    }

    private fun init() {
        activity_login_btnLogin.setOnClickListener { login() }
    }

    private fun login() {
        if (activity_login_etEmail.text.isEmpty() || !activity_login_etEmail.text.toString().isValidEmail()) {
            activity_login_tilEmail.error = "A valid email is required."
            return
        } else {
            activity_login_tilEmail.error = null
        }

        if (activity_login_etPassword.text.isEmpty()) {
            activity_login_tilPassword.error = "Password is required"
            return
        } else {
            activity_login_tilPassword.error = null
        }

        activity_login_progress.showView()
        disableFields()
        presenter.login(activity_login_etEmail.text.toString(), activity_login_etPassword.text.toString())
    }

    override fun dismissProgress() {
        activity_login_progress.goneView()
    }

    override fun onErrorLogin() {
        toastError("Error")
        enableFields()
    }

    override fun onSuccessLogin(user: User) {
        toastSuccess("Successfully Logged In")
        finish()
        startActivity(Intent(this, MainActivity::class.java))
    }

    private fun disableFields() {
        activity_login_etEmail.isEnabled = false
        activity_login_etPassword.isEnabled = false
        activity_login_btnLogin.isEnabled = false
    }

    private fun enableFields() {
        activity_login_etEmail.isEnabled = true
        activity_login_etPassword.isEnabled = true
        activity_login_btnLogin.isEnabled = true
    }

    private fun startAnimation() {
        activity_login_wahoo_icon.let { v ->
            YoYo.with(Techniques.Landing)
                    .onStart { v.showView() }
                    .onEnd { rightToLeft() }
                    .playOn(v)
        }

    }

    private fun rightToLeft() {
        fish_left_to_right.let { v ->
            YoYo.with(Techniques.SlideInLeft)
                    .onStart { v.showView() }
                    .onEnd {
                        v.fadeToHide()
                        leftToMid()
                    }.playOn(v)
        }
    }

    private fun leftToMid() {
        fish_right_to_mid.let { v ->
            YoYo.with(Techniques.SlideInRight)
                    .onStart { v.showView() }
                    .onEnd {
                        v.fadeToHide()
                        fish_mid.fadeToShow()
                    }.playOn(v)
        }
    }

}