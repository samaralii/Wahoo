package com.shaheersolution.wahoo.ui.detail

import android.app.Activity
import android.content.Intent
import android.util.Log
import com.hannesdorfmann.mosby3.mvp.MvpBasePresenter
import com.hannesdorfmann.mosby3.mvp.MvpView
import com.shaheersolution.wahoo.data.models.Invoice
import com.shaheersolution.wahoo.data.models.UpdateInvoice
import com.shaheersolution.wahoo.data.source.TaskDataSource
import com.shaheersolution.wahoo.di.modules.RxSchedulers
import com.shaheersolution.wahoo.util.AppConsts
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.observers.DisposableSingleObserver
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import java.io.File

class DetailPresenter(private val repo: TaskDataSource, private val schedulers: RxSchedulers) : MvpBasePresenter<DetailView>() {

    private val disposable = CompositeDisposable()

    private lateinit var invoice: Invoice
    private lateinit var cacheDir: File

    fun setInvoice(invoice: Invoice?, cacheDir: File) {
        if (invoice != null) {
            this.invoice = invoice
            this.cacheDir = cacheDir
        } else {
            if (isViewAttached) view?.noInvoiceDataFound()
        }
    }

    private fun approveInvoice(fileName: String) {

        if (isViewAttached) view?.showProgress()

        val invoiceId = RequestBody.create(
                MediaType.parse("multipart/form-data"), invoice.id)

        val sub_company_id = RequestBody.create(
                MediaType.parse("multipart/form-data"), invoice.sub_company_id.toString())

        val invoiceNumber = RequestBody.create(
                MediaType.parse("multipart/form-data"), invoice.invoiceNumber)

        val status = RequestBody.create(
                MediaType.parse("multipart/form-data"), 4.toString())

        val signatureFile = File(cacheDir, fileName)
        val requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), signatureFile)
        val signature: MultipartBody.Part = MultipartBody.Part.createFormData("receivingSignature", signatureFile.name, requestFile)
        Log.d("FILE PATH", signatureFile.absolutePath)

        repo.approveInvoice(invoiceId, sub_company_id, status, invoiceNumber, signature)
                .subscribeOn(schedulers.io())
                .observeOn(schedulers.ui())
                .doFinally { if (isViewAttached) view?.dismissProgress() }
                .subscribeWith(object : DisposableSingleObserver<UpdateInvoice>() {
                    override fun onSuccess(t: UpdateInvoice) {

                        try {

                            if (t.response == "success") {

                                if (isViewAttached) view?.onSuccessApprovingInvoice()

                            } else {
                                if (isViewAttached) view?.onErrorApprovingInvoice()
                            }

                        } catch (e: Exception) {
                            e.printStackTrace()
                            if (isViewAttached) view?.onErrorApprovingInvoice()
                        }

                    }

                    override fun onError(e: Throwable) {
                        e.printStackTrace()
                        if (isViewAttached) view?.onErrorApprovingInvoice()
                    }

                })

    }

    fun updateInvoiceStatus(note: String, status: Int) {
        repo.cancelInvoice(invoice.id, invoice.sub_company_id.toString(), status.toString(), note, invoice.invoiceNumber)
                .subscribeOn(schedulers.io())
                .observeOn(schedulers.ui())
                .doFinally { if (isViewAttached) view?.dismissProgress() }
                .subscribeWith(object : DisposableSingleObserver<UpdateInvoice>() {
                    override fun onError(e: Throwable) {
                        e.printStackTrace()
                        if (isViewAttached) view?.onErrorStatusChanged()
                    }

                    override fun onSuccess(t: UpdateInvoice) {

                        try {

                            if (t.response == "success") {
                                if (isViewAttached) view?.onSuccessStatusChanged()
                            } else {
                                if (isViewAttached) view?.onErrorStatusChanged()
                            }

                        } catch (e: Exception) {
                            e.printStackTrace()
                            if (isViewAttached) view?.onErrorStatusChanged()
                        }

                    }

                })
    }


    override fun detachView(retainInstance: Boolean) {
        super.detachView(retainInstance)
        if (!retainInstance) disposable.clear()
    }

    fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == AppConsts.SIGNATURE_ACTIVITY_REQUEST_CODE) {
                val filename = data?.getStringExtra(AppConsts.FILENAME)
                if (filename != null)
                    approveInvoice(filename)
            }
        }
    }
}

interface DetailView : MvpView {
    fun noInvoiceDataFound()
    fun dismissProgress()
    fun onErrorApprovingInvoice()
    fun onSuccessApprovingInvoice()
    fun showProgress()
    fun onErrorStatusChanged()
    fun onSuccessStatusChanged()
}