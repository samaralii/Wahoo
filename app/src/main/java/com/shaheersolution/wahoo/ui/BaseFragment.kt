package com.shaheersolution.wahoo.ui

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.View
import com.hannesdorfmann.mosby3.mvp.MvpBasePresenter
import com.hannesdorfmann.mosby3.mvp.MvpFragment
import com.hannesdorfmann.mosby3.mvp.MvpView

abstract class BaseFragment<V : MvpView, P : MvpBasePresenter<V>> : MvpFragment<V, P>() {

    private var fragmentName = "NoName"
    private val TAG = "WAHOO"

    override fun createPresenter(): P = getPresenter()

    private fun setFragmentName() {
        fragmentName = getFragmentName()
    }

    abstract fun getFragmentName(): String
    abstract fun setPresenter(): P


    override fun onAttach(context: Context?) {
        showLog("onAttach")
        super.onAttach(context)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        showLog("onCreate")
        super.onCreate(savedInstanceState)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        showLog("onViewCreated")
        super.onViewCreated(view, savedInstanceState)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        showLog("onActivityCreated")
        super.onActivityCreated(savedInstanceState)
    }

    override fun onStart() {
        showLog("onStart")
        super.onStart()
    }

    override fun onResume() {
        showLog("onResume")
        super.onResume()
    }

    override fun onPause() {
        showLog("onPause")
        super.onPause()
    }

    override fun onStop() {
        showLog("onStop")
        super.onStop()
    }

    override fun onDestroy() {
        showLog("onDestroy")
        super.onDestroy()
    }

    override fun onDestroyView() {
        showLog("onDestroyView")
        super.onDestroyView()
    }

    override fun onDetach() {
        showLog("onDetach")
        super.onDetach()
    }

    override fun onDestroyOptionsMenu() {
        showLog("onDestroyOptionsMenu")
        super.onDestroyOptionsMenu()
    }

    private fun showLog(methodName: String) {
        Log.i(TAG, "$fragmentName:$methodName")
    }


}