package com.shaheersolution.wahoo.ui.barcodeScanner

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import com.google.zxing.Result
import com.shaheersolution.wahoo.util.AppConsts
import com.tbruyelle.rxpermissions2.RxPermissions
import me.dm7.barcodescanner.zxing.ZXingScannerView

class BarcodeScannerActivity : AppCompatActivity(), ZXingScannerView.ResultHandler {

    lateinit var mScannerView: ZXingScannerView
    private val TAG = BarcodeScannerActivity::class.java.name.toUpperCase()

    private val rxPermission by lazy { RxPermissions(this@BarcodeScannerActivity) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

//        val i = Intent()
//        i.putExtra(AppConsts.BARCODE_VALUE, "0002")
//        setResult(Activity.RESULT_OK, i)
//        finish()


        initScanner()
        rxPermission
                .request(Manifest.permission.CAMERA)
                .subscribe { granted ->
                    if (!granted) finish()
                }
    }

    private fun initScanner() {
        mScannerView = ZXingScannerView(this)
        setContentView(mScannerView)
    }

    override fun onResume() {
        super.onResume()
        mScannerView.apply {
            setResultHandler(this@BarcodeScannerActivity)
            startCamera()
        }
    }

    override fun onPause() {
        super.onPause()
        mScannerView.stopCamera()
    }

    override fun handleResult(rawResult: Result?) {
        Log.v(TAG, rawResult?.text)
        Log.v(TAG, rawResult?.barcodeFormat.toString())

        val intent = Intent().apply {
            putExtra(AppConsts.BARCODE_VALUE, rawResult?.text.toString())
        }

        setResult(Activity.RESULT_OK, intent)
        finish()

        mScannerView.resumeCameraPreview(this)
    }

}