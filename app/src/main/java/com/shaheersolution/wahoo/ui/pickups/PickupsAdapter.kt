package com.shaheersolution.wahoo.ui.pickups

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.google.gson.Gson
import com.shaheersolution.wahoo.R
import com.shaheersolution.wahoo.data.models.Invoice
import com.shaheersolution.wahoo.data.models.InvoicesId

class PickupsAdapter(private val data: MutableList<Invoice>, val showEmptyView: () -> Unit, val itemAlreadyAdded: () -> Unit) : RecyclerView.Adapter<PickupsVH>() {

    private val gson by lazy { Gson() }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PickupsVH {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.list_pickups, parent, false)
        return PickupsVH(v)
    }

    override fun onBindViewHolder(holder: PickupsVH, position: Int) {

        holder.tvName?.text = data[position].receiverName
        holder.tvInvoiceNumber?.text = data[position].invoiceNumber
        holder.tvContactNumber?.text = data[position].contactNo

        holder.btnDelete?.setOnClickListener {
            val pos = holder.layoutPosition
            if (pos >= 0) {
                data.removeAt(pos)
                notifyItemRemoved(pos)
                notifyDataSetChanged()
            }

            if (data.size == 0)
                showEmptyView.invoke()
        }

    }

    override fun getItemCount() = data.size

    fun deleteAllItems() {
        if (data.size > 0) {
            data.clear()
            notifyDataSetChanged()
            showEmptyView.invoke()
        }
    }

    fun addItem(item: Invoice) {

//        data.add(item)
//        notifyDataSetChanged()
//        return

        val isItemAlreadyAdded = (0 until data.size).any { item.id == data[it].id }

        if (!isItemAlreadyAdded) {
            data.add(item)
            notifyDataSetChanged()
        } else {
            itemAlreadyAdded.invoke()
        }

    }


    fun getItems(): String? {

        return if (data.size > 0) {

            val idList = mutableListOf<String>()

            (0 until data.size).forEach { idList.add(data[it].id) }

            val invoicesId = InvoicesId(idList)
            val json = gson.toJson(invoicesId)

            json

        } else null

    }


}

class PickupsVH(v: View) : RecyclerView.ViewHolder(v) {
    val tvInvoiceNumber by lazy { v.findViewById<TextView>(R.id.list_pickups_tvInvoiceNumber) }
    val tvName by lazy { v.findViewById<TextView>(R.id.list_pickups_tvName) }
    val tvContactNumber by lazy { v.findViewById<TextView>(R.id.list_pickups_tvContactNumber) }
    val btnDelete by lazy { v.findViewById<View>(R.id.list_pickups_btnDelete) }

}