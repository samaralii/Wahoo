package com.shaheersolution.wahoo.di.modules

import io.reactivex.Scheduler

/**
 * Created by Sammie on 5/10/2017.
 */

interface RxSchedulers {

    fun io(): Scheduler
    fun computation(): Scheduler
    fun ui(): Scheduler

}
