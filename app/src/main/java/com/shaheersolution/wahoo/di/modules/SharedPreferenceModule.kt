package com.shaheersolution.wahoo.di.modules

import android.content.Context
import android.content.SharedPreferences
import dagger.Module
import dagger.Provides


@Module
class SharedPreferenceModule {

    @Provides
    fun provideSharedPreference(context: Context) = context.getSharedPreferences("com.shaheersolution.wahoo", Context.MODE_PRIVATE)

}