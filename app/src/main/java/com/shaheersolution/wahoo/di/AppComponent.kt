package com.shaheersolution.wahoo.di

import com.shaheersolution.wahoo.App
import com.shaheersolution.wahoo.di.modules.*
import com.shaheersolution.wahoo.ui.delivery.deliveryItems.DeliveryListFragment
import com.shaheersolution.wahoo.ui.delivery.holdItems.HoldListFragment
import com.shaheersolution.wahoo.ui.detail.DetailFragment
import com.shaheersolution.wahoo.ui.login.LoginActivity
import com.shaheersolution.wahoo.ui.main.MainActivity
import com.shaheersolution.wahoo.ui.pickups.PickupsFragment
import dagger.Component
import javax.inject.Singleton


@Singleton
@Component(modules = [(NetModule::class), (AppModule::class), (SchedulersModule::class), (RepositoryModule::class), (SharedPreferenceModule::class)])
interface AppComponent {
    fun Inject(app: App)
    fun Inject(app: LoginActivity)
    fun Inject(pickupsFragment: PickupsFragment)
    fun Inject(deliveryFragment: DeliveryListFragment)
    fun Inject(mainActivity: MainActivity)
    fun Inject(detailFragment: DetailFragment)
    fun Inject(holdListFragment: HoldListFragment)
}