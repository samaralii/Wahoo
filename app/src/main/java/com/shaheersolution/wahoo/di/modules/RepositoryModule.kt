package com.shaheersolution.wahoo.di.modules

import android.content.SharedPreferences
import com.google.gson.Gson
import com.shaheersolution.wahoo.data.source.TaskRepository
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import javax.inject.Singleton

/**
 * Created by Sammie on 4/27/2017.
 */

@Module
class RepositoryModule {

    @Singleton
    @Provides
    fun provideRepository(retrofit: Retrofit, sharedPreferences: SharedPreferences, gson: Gson) =
            TaskRepository(retrofit, sharedPreferences, gson)

}
