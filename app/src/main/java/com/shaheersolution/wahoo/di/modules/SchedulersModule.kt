package com.shaheersolution.wahoo.di.modules


import dagger.Module
import dagger.Provides


@Module
class SchedulersModule {

    @Provides
    fun provideSchedulers() = RxSchedulersImpl()
}
