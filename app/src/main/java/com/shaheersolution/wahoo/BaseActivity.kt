package com.shaheersolution.wahoo

import android.support.annotation.StringRes
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.view.View
import com.shaheersolution.wahoo.util.hideView
import com.shaheersolution.wahoo.util.showView
import kotlinx.android.synthetic.main.include_toolbar.*

open class BaseActivity : AppCompatActivity() {

    public fun gifApplication() = (application as App)

    override fun setContentView(layoutResID: Int) {
        super.setContentView(layoutResID)
        findViewById<Toolbar>(R.id.app_toolbar).let {
            setSupportActionBar(it)
        }
    }

    public fun _setTitle(@StringRes id: Int? = null, title: String? = null, subTitle: String? = null) {

        id?.let {
            val t = getString(it)
            supportActionBar?.title = t
        }

        title?.let { supportActionBar?.title = it }

        subTitle?.let {
            app_toolbar_Subtitle.showView()
            app_toolbar_Subtitle.text = it
        } ?: app_toolbar_Subtitle.hideView()
    }

}