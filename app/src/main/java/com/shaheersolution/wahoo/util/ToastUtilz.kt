package com.shaheersolution.wahoo.util

import android.content.Context
import android.support.v4.app.Fragment
import android.widget.Toast
import es.dmoral.toasty.Toasty


fun Context.toast(msg: String) {
    Toast.makeText(this, msg, Toast.LENGTH_SHORT).show()
}

fun Context.toastSuccess(msg: String) {
    Toasty.success(this, msg, Toast.LENGTH_SHORT).show()
}

fun Context.toastError(msg: String) {
    Toasty.error(this, msg, Toast.LENGTH_SHORT).show()
}

fun Context.toastInfo(msg: String) {
    Toasty.info(this, msg, Toast.LENGTH_SHORT).show()
}

fun Fragment.toast(msg: String) {
    this.context!!.toast(msg)
}

fun Fragment.toastSuccess(msg: String) {
    this.context!!.toastSuccess(msg)
}

fun Fragment.toastError(msg: String) {
    this.context!!.toastError(msg)
}

fun Fragment.toastInfo(msg: String) {
    this.context!!.toastInfo(msg)
}




