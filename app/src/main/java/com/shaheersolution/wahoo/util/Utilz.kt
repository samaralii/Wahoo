package com.shaheersolution.wahoo.util

import android.support.annotation.IdRes
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.Toast
import com.daimajia.androidanimations.library.Techniques
import com.daimajia.androidanimations.library.YoYo


fun View.goneView() {
    this.visibility = View.GONE
}

fun View.hideView() {
    this.visibility = View.INVISIBLE
}

fun View.showView() {
    this.visibility = View.VISIBLE
}

fun View.fadeToHide() {
    YoYo.with(Techniques.FadeOut)
            .duration(200)
            .onEnd { this.hideView() }
            .playOn(this)
}

fun View.fadeToShow() {
    YoYo.with(Techniques.FadeIn)
            .onStart { this.showView() }
            .playOn(this)
}


fun String.isValidEmail() = if (this.isEmpty()) false else android.util.Patterns.EMAIL_ADDRESS.matcher(this).matches()

fun Fragment.replaceFragmentToActivity(fm: FragmentManager, @IdRes id: Int, isBackStack: Boolean = false) {
    fm.beginTransaction().apply {
        replace(id, this@replaceFragmentToActivity)
        if (isBackStack) addToBackStack(null)
        commit()
    }
}

fun Fragment.addFragmentToActivity(fm: FragmentManager, @IdRes id: Int, isBackStack: Boolean = false) {
    fm.beginTransaction().apply {
        add(id, this@addFragmentToActivity)
        if (isBackStack) addToBackStack(null)
        commit()
    }
}


fun Int.getStatus() = when (this) {
    3 -> AppConsts.Status.PROCESSING
    4 -> AppConsts.Status.DELIVERED
    5 -> AppConsts.Status.ON_HOLD
    6 -> AppConsts.Status.CANCELED
    else -> ""
}

fun String.getStatus() = when (this) {
    AppConsts.Status.PROCESSING -> 3
    AppConsts.Status.DELIVERED -> 4
    AppConsts.Status.ON_HOLD -> 5
    AppConsts.Status.CANCELED -> 6
    else -> 0
}

