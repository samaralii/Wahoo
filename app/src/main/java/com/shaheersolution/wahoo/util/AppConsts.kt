package com.shaheersolution.wahoo.util

object AppConsts {

    object Status {
        val PROCESSING = "Processing"
        val DELIVERED = "Delivered"
        val ON_HOLD = "On Hold"
        val CANCELED = "Canceled"

        val ID_PROCESSING = 3
        val ID_DELIVERED = 4
        val ID_ON_HOLD = 5
        val ID_CANCELED = 6
    }

    val BASE_URL = "http://hrspidersystem.com/wahoo/"
//    val BASE_URL = "http://wahooccm.com/"
    val BARCODE_VALUE = "qr_value"
    val BARCODE_ACTIVITY_REQUEST_CODE = 3
    val SIGNATURE_ACTIVITY_REQUEST_CODE = 2
    val USER_DATA = "user_data"
    val TITLE_PICKUPS = "Pickups"
    val FILENAME = "filename"


}